package main

import (
	"fmt"
	"os/user"
)

func sayHello() {
	var userData, err = user.Current()

	if err != nil {
		panic("error, unable to get the current username: " + err.Error())
	} else {
		greeting := "Hello " + userData.Username
		fmt.Println(greeting)
	}
}

func main() {
	sayHello()
}
